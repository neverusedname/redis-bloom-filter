package com.hrvsr.redisbloomfilter;

import org.junit.Test;

public class SimpleHashTest {

	@Test
	public void test() {
		SimpleHashFunction f = new SimpleHashFunction(23, 100000);

		for (int i = 0; i < 10000; i++) {
			String s = "http://www.baidu.com" + i;
			long h = f.hash(s);
			System.out.println(h);
		}
		
		SimpleHashFunction h1 = new SimpleHashFunction(25, 4793);
		SimpleHashFunction h2 = new SimpleHashFunction(35, 4793);
		long n1 = h1.hash("Hello, China Me ae1133ata4");
		long n2 = h2.hash("Hello, China Me ae1133ata4");
		System.out.println(n1);
		System.out.println(n2);
	}
}
