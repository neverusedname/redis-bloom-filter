package com.hrvsr.redisbloomfilter;

import java.io.Closeable;
import java.io.IOException;

import redis.clients.jedis.JedisPool;

public class RedisBloomFilterTest {
	public static void main(String[] args) throws IOException {
//		JedisPool pool = new JedisPool("192.168.0.233");
		 JedisPool pool = new JedisPool("localhost");
		RedisBloomFilter filter = new RedisBloomFilter(pool, 14, "bloomFilter_", 1000, 0.001);
		int N = 1000, falseCounts = 0;
		long t1 = System.currentTimeMillis();
		for (int i = 0; i < 1000; i++) {
			String s = "Hello, China Me ae1133ata" + i;
			boolean in1 = filter.contains(s);
			if (in1) {
				falseCounts++;
			}
			System.out.print("before: " + in1 + "<->");
			filter.add(s);
			boolean in2 = filter.contains(s);
			System.out.println("after: " + in2);
		}
		System.out.println(((double) falseCounts) / N);
		long t2 = System.currentTimeMillis();
		System.out.println("time span: " + (t2 - t1));
		((Closeable) filter).close();
	}
}
