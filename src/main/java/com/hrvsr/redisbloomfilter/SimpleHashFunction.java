package com.hrvsr.redisbloomfilter;

public class SimpleHashFunction implements HashFunction {
	private int seed = 0;
	private long cap = Long.MAX_VALUE;

	public SimpleHashFunction(int seed, long cap) {
		super();
		this.seed = seed;
		this.cap = cap;
	}

	@Override
	public long hash(String charSequence) {
		long s = 0;
		for (int i = 0; i < charSequence.length(); i++) {
			char c = charSequence.charAt(i);
			s = seed * s + c;
		}
		s = s < 0 ? -s : s;
		return s % cap;
	}

}
