package com.hrvsr.redisbloomfilter;

public interface HashFunction {
	public long hash(String charSequence);
}
