package com.hrvsr.redisbloomfilter;

public interface BloomFilter {
	public boolean contains(String charSequence);
	public void add(String charSequence);
}
